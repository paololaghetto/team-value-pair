
package it.unipd.dei.bdc1718;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.mllib.linalg.Vector;
import org.apache.spark.mllib.linalg.Vectors;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class G24HM3 {

    public static void main(String[] args) throws IOException {
        if (args.length == 0) {
            throw new IllegalArgumentException("Expecting the file name on the command line");
        }

        InputOutput io = new InputOutput();
        ArrayList<Vector> in = io.readVectorsSeq(args[0]);

        Scanner s = new Scanner(System.in);
        System.out.print("Insert the number of clusters k: ");
        int k = s.nextInt();
        int k_1= Integer.MIN_VALUE;

        // Check if k_1 is greater than k, otherwise reinsert it
        while (k>k_1)
        {
            System.out.print("Insert the number of clusters k_1: ");
            k_1 = s.nextInt();
        }

        // Run kcenter algorithm
        long start = System.currentTimeMillis();
        ArrayList<Vector> fromKcenter = kcenter(in, k);
        long end = System.currentTimeMillis();

        System.out.println("*** k-center terminated after: " + (end - start) + " ms");

        // Create an array of weight with all the values equal to 1
        ArrayList<Double> w = new ArrayList<>();
        for (int i = 0; i < in.size(); i++) {
            w.add(1.0);
        }

        // Run kmeansPP algorithm
        long start2 = System.currentTimeMillis();
        ArrayList<Vector> fromKmeansPP = kmeansPP(in, w, k);
        long end2 = System.currentTimeMillis();

        System.out.println("*** k-means++ terminated after: " + (end2 - start2) + " ms");

        // Compute the average squared distance of a point of P from its closest center computed with kmeansPP algorithm
        double obj = kmeansObj(in, fromKmeansPP);
        System.out.println("The result of the objective function is: " + obj);

        // Finally use kcenter output as input for kmeansPP and then compute the average squared distance of a point of P from its closest center
        ArrayList<Vector> X = kcenter(in, k_1);

        ArrayList<Double> wc = new ArrayList<>();
        for (int i = 0; i < X.size(); i++) {
            wc.add(1.0);
        }

        ArrayList<Vector> C = kmeansPP(X , wc, k);
        double obj2 = kmeansObj(in, C);
        System.out.println("The result of the objective function is: " + obj2);


    }

    public static ArrayList<Vector> kcenter(ArrayList<Vector> Pi, int k) {

        // Initialize S and make a copy of P to work on
        ArrayList<Vector> S = new ArrayList<Vector>();
        ArrayList<Vector> P = new ArrayList<Vector>(Pi);
        ArrayList<Double> curMinDist = new ArrayList<>();
        Random r = new Random();
        double curMax = Double.MIN_VALUE;
        int curMaxId = -1;

		// Select a random center and remove it from P then add it to S
        S.add(P.remove(r.nextInt(P.size())));

        // Repeat until the number k of centers is reached
        for (int i = 1; i <= k; i++) {

            curMax = Double.MIN_VALUE;
            curMaxId = -1;

            // Compute the distances of each point from the last added center
            for (int j = 0; j < P.size(); j++) {

                double curDist = Math.sqrt(Vectors.sqdist(P.get(j), S.get(i-1)));

                // If a smaller distance is found for the current center change the stored value
				// If it is the first iteration then add the entries instead
                if (S.size() == 1) {
                    curMinDist.add(curDist);
                } else if (curDist < curMinDist.get(j)) {
                    curMinDist.set(j, curDist);
                }

                // Keep track of the max distance and its id
                if (curMinDist.get(j) > curMax) {
                    curMax = curMinDist.get(j);
                    curMaxId = j;
                }
            }

            // Add the new center to S while removing it from P
            S.add(P.remove(curMaxId));
            // Also remove the distance of that center from the arraylist
            curMinDist.remove(curMaxId);

        }

        return S;
    }

    public static ArrayList<Vector> kmeansPP(ArrayList<Vector> Pi, ArrayList<Double> WPi, int k) {

        // Initialize S and make a copy of P and WP to work on
        ArrayList<Vector> S = new ArrayList<Vector>();
        ArrayList<Vector> P = new ArrayList<Vector>(Pi);
        ArrayList<Double> WP = new ArrayList<>(WPi);
        ArrayList<Double> curMinDist = new ArrayList<>();
        Random r = new Random();
        double distSum = 0, randNum;
        double[] probDist;

        // Add to S an initial random center from P and remove it from P
        S.add(P.remove(r.nextInt(P.size())));

        // In the first iteration add the entries, otherwise just modify them
        for (int i = 1; i <= k; i++) {

            distSum = 0;

            // Compute the distances of each point from the last center
            for (int j = 0; j < P.size(); j++) {
                double curDist = Math.sqrt(Vectors.sqdist(P.get(j), S.get(i-1)));

                // In the first iteration add the entries, otherwise just modify them
                if (S.size() == 1) {
                    curMinDist.add(curDist);
                } else if (curDist < curMinDist.get(j)) {
                    // If a smaller distance is found update curMinDist
                    curMinDist.set(j, curDist);
                }

                // Compute the weighted sum of squared distances
                distSum += Math.pow(curMinDist.get(j), 2) * WP.get(j);
            }

            // Initialize the probability distribution
            probDist = new double[P.size()];
            for (int j = 0; j < P.size(); j++) {
                probDist[j] = WP.get(j) * Math.pow(curMinDist.get(j), 2) / distSum;
            }

            // Draw a point according to the new-found probability distribution
            randNum = r.nextDouble();
            double leftSum = 0;
            double rightSum = probDist[0];
            for (int j = 1; j <= P.size(); j++) {

                if ((leftSum <= randNum && randNum <= rightSum) || (j == P.size()))
                {
					// A new point is found add it to S while removing it from the other arraylists
                    S.add(P.remove(j-1));
                    curMinDist.remove(j-1);
                    WP.remove(j-1);
                    break;
                }
				
				// Update the values of the sums
                leftSum = rightSum;
                rightSum += probDist[j];
            }
        }
		
        return S;
    }


    public static double kmeansObj(ArrayList<Vector> P, ArrayList<Vector> C) {
        double sumDist = 0;
        double closest = Double.MAX_VALUE;
        double dist;

        // For every point in P find its nearest center and then sum up into sumDist variable
        for (int i = 0; i < P.size(); i++)
        {
            for (int j = 0; j < C.size(); j++)
            {
                dist = Vectors.sqdist(P.get(i), C.get(j));
                if (dist < closest)
                {
                    closest = dist;
                }
            }
            sumDist += closest;
            closest = Double.MAX_VALUE;
        }
        return sumDist/(P.size());
    }
}